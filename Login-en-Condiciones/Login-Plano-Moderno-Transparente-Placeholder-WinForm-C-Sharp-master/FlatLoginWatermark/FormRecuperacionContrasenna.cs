﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace FlatLoginWatermark
{
    public partial class FormRecuperacionContrasenna : Form
    {
        private SqlConnection conexion;

        //Constructor
        public FormRecuperacionContrasenna(SqlConnection conexion)
        {
            InitializeComponent();
            this.conexion = conexion;
        }


        // ---------------------------------
        //      EVENTOS BOTONES
        // ---------------------------------

        //Botón enviar
        private void btnEnviar_Click(object sender, EventArgs e)
        {
            if(txtEmail.TextLength > 0)
                comprobacionExisteUsuario();
            else
                MessageBox.Show("Inserte primero su Email", "INFORMACIÓN", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        //Botón cerrar
        private void btncerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        // ---------------------------------
        //      MÉTODOS AUXILIARES
        // ---------------------------------

        // Nos comprueba a través del Email si existe el usuario
        // Y si así fuere, envíar los datos al formulario de edición de la clave
        private void comprobacionExisteUsuario()
        {
            SqlCommand cmd;
            SqlDataReader dataReader;
            string usuario = "";
            string email = "";
            string nombre = "";

            cmd = new SqlCommand("SELECT NOMBREUSUARIO, EMAIL, NOMBREC FROM Usuario where EMAIL = '" + txtEmail.Text + "'", this.conexion);
            dataReader = cmd.ExecuteReader();

            while (dataReader.Read())
            {
                usuario = dataReader["NOMBREUSUARIO"].ToString();
                email = dataReader["EMAIL"].ToString();
                nombre = dataReader["NOMBREC"].ToString();
            }
            dataReader.Close();

            if (datosEncontrados(usuario, email, nombre))
            {
                FormAltaUsuario fau = new FormAltaUsuario("cambioClave", usuario, email, nombre, this.conexion);
                fau.ShowDialog();
            }
            else
                MessageBox.Show("¡DATOS NO ENCONTRADOS!" +
                    "\nCompruebe que su Email es correcto", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }



        // Comprobamos que los datos han sido encontrados
        private bool datosEncontrados(string usuario, string email, string nombre)
        {
            if (usuario.Length > 0 && email.Length > 0 && nombre.Length > 0)
                return true;
            else
                return false;
        }

        
    }
}
