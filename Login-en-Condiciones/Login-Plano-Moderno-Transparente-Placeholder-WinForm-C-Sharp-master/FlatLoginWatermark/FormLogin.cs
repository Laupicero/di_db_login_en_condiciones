﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Data.SqlClient;

namespace FlatLoginWatermark
{
    public partial class FormLogin : Form
    {
        private SqlConnection conexion;


        // LOAD EVENT
        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                string cadenaDeConexion = @"Server=DESKTOP-BK0GI3H;Database=BDPasaje; uid=sa; pwd=2DAM2dam#";
                this.conexion = new SqlConnection(cadenaDeConexion);
                conexion.Open();
                MessageBox.Show("Conexion exitosa a SQL Server");
            }
            catch (Exception ex)
            {
                MessageBox.Show("No s epudo habiitar la conexión a SQL Server");
            }
            
        }

        //Constructor
        public FormLogin()
        {
            InitializeComponent();
        }


        // ---------------------------------
        //      EVENTOS BOTONES
        // ---------------------------------
        
        //Botón para hacer Login
        private void btnlogin_Click(object sender, EventArgs e)
        {

            if (comporbarCamposRellenos())
            {
                comprobarExisteUsuario(txtuser.Text, txtpass.Text);
            }
            else
            {
                MessageBox.Show("No están todos los campos rellenos correctamente, por favor inserte sus datos en ambos", "INFORMACIÓN", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        //Botón minimizar
        private void btnminimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }


        //Botón para recuperar la contraseña
        private void linkpass_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            AbrirFromRecupContrasenna();
        }

        // Botón para abrir el formulario de Registro de usuario (en modo registro)
        private void linkLblRegistro_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            FormAltaUsuario fau = new FormAltaUsuario("registro", this.conexion);
            fau.ShowDialog();
        }


        // Botón cerrar. Nos cierra la aplicación
        private void btncerrar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        


        // ---------------------------------
        //      MÉTODOS AUXILIARES
        // ---------------------------------

        // Nos comprobará si están todos los campos rellenos del formulario
        // TRUE: Están rellenos
        // FALSE: Falta algún campo
        private bool comporbarCamposRellenos()
        {
            if (txtuser.TextLength > 0 || txtpass.TextLength > 0)
                return true;
            else
                return false;
        }

        // Al hacer 'click' en éste nos limpia el textBox del usuario
        private void txtuser_Click(object sender, EventArgs e)
        {
            if(txtuser.Text == "Usuario")
                txtuser.Clear();
        }

        // Al hacer 'click' en éste nos limpia el textBox de la contraseña
        private void txtpass_Click(object sender, EventArgs e)
        {
            if (txtpass.Text == "Contraseña")
                txtpass.Clear();
        }

        //Nos comprueba si existe el usuario
        private void comprobarExisteUsuario(string nombreUser, string passUser)
        {

            switch(datosUsuarioCorrectos(nombreUser, passUser))
            {
                case 0:
                    MessageBox.Show("No se ha encontrado a el usuario.\nContemple por favor las mayúsculas y minúsculas de su nombre de usuario", "INFORMACIÓN", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;

                case 1:
                    MessageBox.Show("El nombre de usuario es correcto pero la contraseña no, por favor, pongáse en contacto con nosotros para cambiarla", "INFORMACIÓN", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;

                case 2:
                    FormBienvenida bienvenidaForm = new FormBienvenida(nombreUser, this.conexion);
                    bienvenidaForm.ShowDialog();
                    break;

                default:
                    MessageBox.Show("Opción no contemplada", "INFORMACIÓN", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
            }
        }

        // Nos busca el usuario y la contraseña en nuestra DB
        // Y nos dice si los datos son correctos o no
        // Nos devolverá una valiable de tipo entero, y dependiendo del valor de ésta, el resultado será:
        // 0 -> No se ha encontrado ni el usuario ni la contraseña
        // 1 -> Se ha encontrado al usuario, pero la contraseña no es correcta
        // 2 -> Ambos datos son correctos
        private int datosUsuarioCorrectos(string nombreUser, string passUser)
        {
            SqlCommand cmd;
            SqlDataReader dataReader;
            int result = 0;
            String nombre = "";
            String pass = "";

            cmd = new SqlCommand("SELECT NOMBREUSUARIO, CONTRA FROM Usuario WHERE NOMBREUSUARIO='" +nombreUser+ "'", this.conexion);
            dataReader = cmd.ExecuteReader();

            while (dataReader.Read())
            {
                nombre = dataReader["NOMBREUSUARIO"].ToString();
                pass = dataReader["CONTRA"].ToString();
            }
            dataReader.Close();

            if (nombre == nombreUser)
                result = 1;
            if (nombre == nombreUser && pass == passUser)
                result = 2;
            return result;
        }


        //Nos abre la ventana e WindowsForms para poder recuperar la contraseña
        private void AbrirFromRecupContrasenna()
        {
            FormRecuperacionContrasenna frc = new FormRecuperacionContrasenna(this.conexion);
            frc.Show();
        }

        
    }
}
